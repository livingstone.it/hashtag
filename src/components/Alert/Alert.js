import React from "react";

const Alert = (props) => {

    const onClose = () => {
        if(document.getElementById(props.alertType+'Alert')){
            document.getElementById(props.alertType+'Alert').className = "d-none";
        }
    }
  return (
    <div>
      {props.alertType === 'success' && (
        <div
            id="successAlert"
          className="alert alert-success alert-dismissible fade show fixed-bottom"
          role="alert"
        >
          <p>You have been signed up successfully! </p>
          <button
            type="button"
            className="close"
            data-dismiss="alert"
            aria-label="Close"
            onClick={onClose}
          >
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
      )}
      {props.alertType === 'error' &&
      (
        <div
        id="errorAlert"
        className="alert alert-success alert-dismissible fade show fixed-bottom"
        role="alert"
      >
        <p>Network Error! Please try again later </p>
        <button
          type="button"
          className="close"
          data-dismiss="alert"
          aria-label="Close"
          onClick={onClose}
        >
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      )}
    </div>
  );
};

export default Alert;
