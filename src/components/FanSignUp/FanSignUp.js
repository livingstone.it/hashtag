import React from "react";
import axios from 'axios';
import Alert from "../Alert/Alert";

const FanSignUp = (props) => {
  
    const [formData, setFormData] = React.useState({
        firstName: '',
        lastName: '',
        userName: '',
        email: '',
        password: '',
        timeZone: ''
    });

    const [alertType, setAlertType] = React.useState('');
    const [formError, setFormError] = React.useState(false);
    const onFormSubmission = (e) => {
        e.preventDefault();
        if(formError){
            if(document.getElementById(String('formError'))){
                document.getElementById(String('formError')).className = 'd-inline text-danger';
            }
        }
        else{
            axios.post('http://134.209.148.76:2000/api/v3/sign-up/fan', {
            "first_name": formData.firstName,
            "last_name": formData.lastName,
            "username": formData.userName,
            "email": formData.email,
            "password": formData.password,
            "timezone": formData.timeZone,
            "captcha": true
        })
        .then( (res)=> {
            console.log(res);
            setAlertType('success');
        })
        .catch( err => {
            console.error(err);
            setAlertType('error');
        })
        }
        
  };

  const checkEmailValidity = (id, value) =>{
        const regex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if(regex.test(String(value).toLowerCase())){
            if(document.getElementById(String(id+'Error'))){
                document.getElementById(String(id+'Error')).className = 'd-none';
            }
            setFormError(false);
            return true;
        }
        else{
            if(document.getElementById(String(id+'Error'))){
                document.getElementById(String(id+'Error')).className = 'd-inline text-danger';
            }
            setFormError(true);
            return false;
        }

  }
  const checkNameValidity = (id, value) => {
      var regex = /^[a-zA-Z]*$/;

      if(regex.test(value)){
        if(document.getElementById(String(id+'Error'))){
            document.getElementById(String(id+'Error')).className = 'd-none';
        }
        setFormError(false);
         return true;
      }
      else{
        if(document.getElementById(String(id+'Error'))){
            document.getElementById(String(id+'Error')).className = 'd-inline text-danger';
        }
        
        setFormError(true);
        return false;
      }
  }
  const onFormFieldChange = (e) => {
      e.preventDefault();
      if(e.target.id === 'firstName' || e.target.id === 'lastName'){
        if(checkNameValidity(e.target.id, e.target.value)){
            setFormData({...formData, [e.target.id]: e.target.value});
          }
      }
      if(e.target.id === 'email'){
        if(checkEmailValidity(e.target.id, e.target.value)){
            setFormData({...formData, [e.target.id]: e.target.value});
          }
      }
  }

  return (
    <div>
        <Alert alertType={alertType}/>
      <div className="container w-50">
        <div className="row">
          <div className="column d-flex justify-content-center">
            <h2 className="text-center m-1 p-1">Fan Signup</h2>
          </div>
        </div>
        <form className="m-1 p-3 min-vw-50 rounded border border-success border-1">
        <small id="formError" className="d-none">Please resolve the errors</small>
          <div className="row">
            <div className="column mb-3">
              <label htmlFor="firstName" className="form-label">
                First Name
              </label>
            </div>
          </div>
          <div className="row">
            <div className="column mb-3">
              <input
                onChange={onFormFieldChange}
                type="text"
                className="form-control min-vw-50 p-2"
                id="firstName"
                aria-describedby="emailHelp"
                required
              />
              <small id="firstNameError" className="d-none text-danger">Alphabets only</small>
            </div>
          </div>
          <div className="row">
            <div className="column mb-3">
              <label htmlFor="lastName" className="form-label">
                Last Name
              </label>
            </div>
          </div>
          <div className="row">
            <div className="column mb-3">
              <input
                onChange={onFormFieldChange}
                type="text"
                className="form-control min-vw-50 p-2"
                id="lastName"
                aria-describedby="emailHelp"
                required
              />
              <small id="lastNameError" className="d-none text-danger">Alphabets only</small>
            </div>
          </div>
          <div className="row">
            <div className="column mb-3">
              <label htmlFor="userName" className="form-label">
                Username
              </label>
            </div>
          </div>
          <div className="row">
            <div className="column mb-3">
              <input
                onChange={onFormFieldChange}
                type="text"
                className="form-control min-vw-50 p-2"
                id="userName"
                aria-describedby="userNameHelp"
                required
              />
            </div>
          </div>
          <div className="row">
            <div className="column mb-3">
              <label htmlFor="email" className="form-label">
                Email address
              </label>
            </div>
          </div>
          <div className="row">
            <div className="column mb-3">
              <input
                onChange={onFormFieldChange}
                type="email"
                className="form-control min-vw-50 p-2"
                id="email"
                aria-describedby="emailHelp"
                required
              />
              <small id="emailError" className="d-none text-danger">Please enter a valid email</small>
            </div>
          </div>
          <div className="row">
            <div className="column mb-3">
              <label htmlFor="password" className="form-label">
                Password
              </label>
            </div>
          </div>
          <div className="row">
            <div className="column mb-3">
              <input
                onChange={onFormFieldChange}
                type="password"
                className="form-control"
                id="password"
                required
              />
              
            </div>
          </div>
          <div className="row">
            <div className="column mb-3">
              <label htmlFor="password" className="form-label">
                Timezone
              </label>
            </div>
          </div>
          <div className="row">
            <div className="column mb-3">
              <input
                onChange={onFormFieldChange}
                type="text"
                className="form-control"
                id="timeZone"
                required
              />
            </div>
          </div>

          <div className="row w-50">
            <div className="column mb-3">
              <button
                onClick={onFormSubmission}
                type="submit"
                className="btn btn-primary"
              >
                Submit
              </button>
            </div>
          </div>
        </form>
      </div>
    </div>
  );
};

export default FanSignUp;
