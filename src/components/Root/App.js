import "./App.css";
import * as React from "react";
import FanSignUp from './../FanSignUp/FanSignUp';
import TalentSignUp from './../TalentSignUp/TalentSignUp';

const App = () => {
  const talentTabButtonRef = React.useRef(null);
  const fanTabButtonRef = React.useRef(null);
  const talentTabContentRef = React.useRef(null);
  const fanTabContentRef = React.useRef(null);

  const onTalentTabClick = () => {
    console.log("talent tab clicked");
    talentTabButtonRef.current.className = "nav-link active";
    fanTabButtonRef.current.className = "nav-link";

    talentTabContentRef.current.className = "tab-pane fade show active";
    fanTabContentRef.current.className = "tab-pane fade";
  };

  const onFanTabClick = () => {
    console.log("fan tab clicked");
    talentTabButtonRef.current.className = "nav-link";
    fanTabButtonRef.current.className = "nav-link active";

    talentTabContentRef.current.className = "tab-pane fade";
    fanTabContentRef.current.className = "tab-pane fade show active";
  };

  return (
    <div>
      <ul className="nav nav-tabs" id="myTab" role="tablist">
        <li className="nav-item" role="presentation">
          <button
            className="nav-link active"
            id="home-tab"
            ref={talentTabButtonRef}
            onClick={onTalentTabClick}
            data-bs-toggle="tab"
            data-bs-target="#home"
            type="button"
            role="tab"
            aria-controls="home"
            aria-selected="true"
          >
            Talent Signup
          </button>
        </li>
        <li className="nav-item" role="presentation">
          <button
            className="nav-link"
            id="profile-tab"
            ref={fanTabButtonRef}
            onClick={onFanTabClick}
            data-bs-toggle="tab"
            data-bs-target="#profile"
            type="button"
            role="tab"
            aria-controls="profile"
            aria-selected="false"
          >
            Fan Signup
          </button>
        </li>
        
      </ul>
      <div className="tab-content" id="myTabContent">
        <div
          ref={talentTabContentRef}
          className="tab-pane fade show active"
          id="home"
          role="tabpanel"
          aria-labelledby="home-tab"
        >
          <TalentSignUp />
        </div>
        <div
          ref={fanTabContentRef}
          className="tab-pane fade"
          id="profile"
          role="tabpanel"
          aria-labelledby="profile-tab"
        >
          <FanSignUp />
        </div>
    
        </div>
      </div>
  );
};

export default App;
