import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from '../src/components/Root/App';
import '../src/styles/css/bootstrap-grid.min.css';
import '../src/styles/css/bootstrap-reboot.min.css';
import '../src/styles/css/bootstrap-utilities.min.css';
import '../src/styles/css/bootstrap.min.css';
ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);
